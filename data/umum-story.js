const UmumStory = [
  {
    id: 1,
    title: 'Mengantri di Supermarket Saat Pandemi Covid-19',
    category: 'Humor Umum',
    content: 
    `Saya sedang mengantri di supermarket ketika seorang wanita di depan saya kentut.

      Saya marah... Tetapi sebelum saya mengatakan sesuatu, dia berbalik dan berkata,
      
      "Jika Anda mendengar itu, maka Anda tidak menjaga jarak..."
      
      Kemudian dia melanjutkan,
      
      "Dan jika Anda mencium baunya, maka masker wajah Anda tidak membantu sama sekali..."
    `
  },
  {
    id: 2,
    title: 'Ketika Kambing Berdamai dengan Singa',
    category: 'Humor Umum',
    content: 
    `Alkisah, Jono dan istrinya pergi ke kebun binatang, lalu melihat sesuatu yang menarik di sebuah kandang. Di situ terlihat seekor kambing yang tinggal bersama dengan seekor singa di dalam sebuah kandang. Tampak si kambing dengan santai makan daun-daunan yang sudah disiapkan di keranjang, sementara singa dengan tenang sibuk dengan aktivitasnya sendiri.

      Melihat hal itu, istri Jono berkata, "Lihat Pa, apa Papa gak sadar, setiap kali kita ke sini, itu adalah sebuah pelajaran bagi kita, di mana makhluk hidup bisa berdampingan dan berdamai meskipun secara hukum alam seharusnya bermusuhan, karena salah satunya merupakan pemangsa yang lain."
      
      Jono juga melihat dengan takjub, "Betul, Ma. Papa juga baru menyadari sekarang, ini sebuah pelajaran bagaimana kita juga bisa berdamai dengan keadaan, sakit penyakit, ancaman lain, yang bisa membahayakan hidup kita."
      
      Tak jauh dari mereka, ada petugas kebun binatang sedang melakukan perawatan rutin di kandang itu, lalu Jono bertanya, "Mas, siapa yang melatih singa ini kok bisa seperti itu?"
      
      Petugas menjawab, "Kami tidak melatih dan tidak melakukan apapun."
      
      Jono penasaran, lalu bertanya, "Lalu bagaimana kok setiap kami ke sini, singa dan kambing ini baik-baik saja? Apa rahasianya?"
      
      Petugas menjawab, "Oh, itu ya. Jadi begini, setiap pagi sebelum membuka kebun binatang ini untuk umum, kami membersihkan kandang, dan tulang belulang kambing yang kemarin kami bersihkan, lalu kami isi ulang dengan kambing baru..."
    `
  },
  {
    id: 3,
    title: 'Masih Terlalu Dekat dengan Bola',
    category: 'Humor Umum',
    content: 
    `Saya mengambil kursus golf pada suatu hari mencoba memperbaiki permainannya.

    Pemain profesional tua ini duduk di tempat kursus sambil memberi pelajaran dan setelah setiap ayunan, dia berkata: "Anda berdiri terlalu dekat dengan bola".
    
    Jadi saya menyesuaikan posisi saya dan mengayunkan tongkatnya lagi.
    
    Lagi Pemain profesional golf melihat dari kursinya dan mengatakan hal yang sama, "Anda terlalu dekat dengan bola."
    
    Jadi saya mundur sedikit dan mengayun.
    
    Ini berlangsung selama enam ayunan lagi dengan saran yang sama dan akhirnya, karena putus asa Jono berteriak, "Apa yang Anda bicarakan!"
    
    Pemain profesional tua itu dengan santai berkata, "Tidak, tidak, masalahnya Anda masih terlalu dekat dengan bola setelah Anda memukulnya."
    `
  },
  {
    id: 4,
    title: 'Belajar Bermain Golf',
    category: 'Humor Umum',
    content: 
    `Seorang pensiunan diberikan satu set tongkat golf oleh rekan kerjanya.

    Karena dia ingin mencoba olahraga ini, dia meminta pelajaran pemain profesional setempat, dan menjelaskan bahwa dia tidak tahu apa-apa tentang permainan golf.
    
    Pemain profesional menunjukkan kepadanya cara mengayun tongkat golf, lalu berkata, "Pukul saja bola ke arah bendera hijau pertama yang ada di sana."
    
    Pemula itu memulai dan memukul bola lurus ke fairway dan menuju green, tempat bola itu berhenti beberapa inci dari lubang.
    
    "Sekarang apa?" orang itu bertanya pada pemain profesional yang tidak bisa berkata-kata karena takjub.
    
    "Eh... Anda seharusnya memukul bola masuk ke dalam lubang," akhirnya pemain profesional bisa mulai berkata-kata, setelah dia mulai bisa berbicara lagi.
    
    Pensiunan itu menjawab, "Oh, bagus! KENAPA TIDAK DARI AWAL TADI KAMU MEMBERITAHUKU ??!!!"
    `
  },
  {
    id: 5,
    title: 'Berikutnya adalah Giliranmu',
    category: 'Humor Umum',
    content: 
    `Jono memiliki 3 bibi dan 4 paman, di mana dalam setiap upacara pernikahan, mereka selalu mengejek Jono: "Wah, Jon kamu sudah cukup berumur, jadi kami harap berikutnya adalah giliranmu."

    Untuk menjawab hal itu, maka setiap ada pemakaman, Jono gantian memberi tahu mereka: "Aduh, om sama tante sudah cukup berumur, jadi saya harap lain kali giliran kalian!"
    `
  },
]

export default UmumStory