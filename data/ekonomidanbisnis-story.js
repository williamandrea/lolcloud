const EkonomidanBisnisStory = [
  {
    id: 1,
    title: 'Memiliki Ingatan Sangat Lemah',
    category: 'Humor Ekonomi dan Bisnis',
    content: 
    `Ingatan saya sangat buruk sehingga menyebabkan saya kehilangan pekerjaan.

    Namun, saat ini saya masih bekerja.
    
    Saya hanya tidak ingat saya bekerja di mana.
    `
  },
  {
    id: 2,
    title: 'Pemakaman Seorang Akuntan Senior',
    category: 'Humor Ekonomi dan Bisnis',
    content: 
    `Seorang lelaki tua adalah seorang manajer akuntansi di sebuah perusahaan.

    Setiap hari ketika dia datang ke kantor, yang pertama kali dilakukan di mejanya yaitu: dia membuka laci, melihat suatu kertas kecil di dalamnya dengan sangat hati-hati, memeriksanya berulang-ulang lalu dia memasukkan kertas itu dan menutup laci itu kembali.
    
    Setelah dua puluh tahun bekerja di posisi yang sama, suatu hari dia meninggal.
    
    Setelah pemakamannya, rekan-rekannya masuk ke ruangan di kantornya untuk memeriksa apa sebenarnya yang ada di laci itu, mereka membuka laci, ada selembar kertas yang tebal itu tertulis dengan jelas, "Debit di kiri, Kredit di kanan!"
    `
  },
  {
    id: 3,
    title: 'Semua Orang di Dunia Membenci',
    category: 'Humor Ekonomi dan Bisnis',
    content: 
    `Seorang pegawai Kantor Pajak berbaring di sofa psikiaternya meratapi kenyataan bahwa semua orang di dunia membencinya.

    "Omong kosong", kata dokternya. "Semua orang di dunia tidak membencimu. Semua orang di Indonesia, mungkin, tapi tentu saja tidak semua orang di dunia."
    `
  },
  {
    id: 4,
    title: 'Pemakaman Seorang Penasihat Keuangan',
    category: 'Humor Ekonomi dan Bisnis',
    content: 
    `Seorang pria berada di pemakaman seorang penasihat keuangan dan terkejut dengan jumlah orang yang melayat untuk pria yang satu ini.

    Dia menoleh ke orang-orang di sekitarnya.
    
    "Kenapa kalian semua menghadiri pemakaman pria ini?"
    
    Seorang pria berbalik ke arahnya dan berkata, "Kita semua adalah klien."
    
    "Dan kalian semua datang untuk memberikan penghormatan? Menyentuh sekali."
    
    "Tidak, kami datang untuk memastikan dia sudah mati."
    `
  },
  {
    id: 5,
    title: 'Pensiun Setelah Bekerja 40 Tahun',
    category: 'Humor Ekonomi dan Bisnis',
    content: 
    `Setelah 40 tahun bekerja keras, seorang pria pensiun dengan uang 5 Milyar yang diperolehnya melalui keberanian, ketekunan, inisiatif, keterampilan, pengabdian kepada tugas, penghematan, efisiensi, investasi yang cerdas.

    Serta dari kematian pamannya yang mewariskan Rp.4.999.900.000,-.
    `
  },
]

export default EkonomidanBisnisStory