const Politik = [
  {
    id: 1,
    title: 'Dampak Mati Listrik di Jakarta',
    category: 'Humor Politik',
    content: 
    `Dampak mati listrik di Jakarta, Presiden mendatangi kantor PLN, menemui Plt. Dirut PLN, untuk menanyakan masalah listrik mati kemarin. Setelah mendapat penjelasan, Presiden memberi instruksi agar listrik segera dipulihkan. "Sekalian, itu pemadaman listrik bergilir di daerah-daerah, segera carikan solusinya," perintah Presiden.

    "Kalau itu sudah bisa kami atasi, Pak," kata Plt. Dirut yakin.
    
    "Gimana solusinya?"
    
    "Sekarang sistemnya kami ubah, dari pemadaman bergilir menjadi penyalaan bergilir."
    `
  },
  {
    id: 2,
    title: 'Habis Pulang dari Kuburan',
    category: 'Humor Politik',
    content: 
    `Dua teman bertemu di jalan.

    "Dari mana kamul?" Tanya Jono.
    
    "Oh, jangan tanya saya! Saya datang dari kuburan. Kami baru saja menguburkan pejabat yang berasal dari daerah kami," jawab Toni.
    
    "Maafkan aku!" Kata Jono, "Tapi mengapa wajahmu luka tergores begitu?"
    
    "Itu tidak mudah!" Kata Toni, "Dia ngotot tidak mau dikubur!"
    `
  },
  {
    id: 3,
    title: 'Kata Kunci dalam Pemilihan Umum',
    category: 'Humor Politik',
    content: 
    `Kata kunci dari pemilihan umum saat ini adalah "PERUBAHAN."

    Para calon melemparkan kata ini tanpa mengatakan apa yang ingin mereka ubah. Hanya saja kita perlu PERUBAHAN! Ini mengingatkan kita pada ilustrasi berikut.
    
    Bertahun-tahun yang lalu, ada kisah lama di Marinir tentang seorang letnan yang memeriksa Marinirnya dan mengatakan kepada komandan peleton bahwa mereka berbau busuk. Letnan itu akhitnya menyarankan perubahan agar mereka mengganti pakaian dalam mereka.
    
    Komandan peleton menjawab, "Siap, Pak. Saya akan segera memeriksanya."
    
    Dia masuk ke tenda dan berkata, "Letnan menganggap kalian bebau busuk, dan dia ingin perubahan agar kalian mengganti celana dalam kalian. Jono, kau berganti dengan punya Budi, Nando, kau berganti dengan punya Anton, Banu, kau berganti dengan punya Totok... Ganti, mulai!"
    
    Dan moral cerita ini adalah: Seorang kandidat mungkin menjanjikan perubahan di Ibukota... tetapi baunya busuknya akan tetap sama saja!
    `
  },
  {
    id: 4,
    title: 'Menyelamatkan Seorang Politisi',
    category: 'Humor Politik',
    content: 
    `Tiga orang anak laki-laki berjalan melewati hutan dan tiba-tiba mendengar jeritan minta tolong.

    Mereka mengikuti suara ke danau dan melihat seorang pejabat yang menjadi tersangka korupsi tenggelam.
    
    Anak-anak itu melompat ke dalam air dan membawanya ke tepian. Pejabat bertanya kepada anak-anak itu bagaimana dia bisa membayar mereka.
    
    Anak laki-laki pertama berkata, "Saya ingin HP baru."
    
    Anak laki-laki kedua berkata, "Saya ingin sepeda motor."
    
    Anak ketiga berkata, "Saya ingin batu nisan yang bagus."
    
    PEjabat itu kaget, lalu bertanya, "Mengapa demikian?"
    
    Anak itu berkata, "Karena ketika ayah saya tahu saya membantu menyelamatkan Anda, dia akan membunuh saya."
    `
  },
  {
    id: 5,
    title: 'Bangga Bendera Merah Putih',
    category: 'Humor Politik',
    content: 
    `Banggalah jadi orang Indonesia. Kita beruntung bendera kita Merah Putih. Jadi, komandan upacara cukup memberi aba-aba:

    "Kepada Sang Merah Putih, hormaaat graaakkk!!"
    
    Coba kalau di Amerika, maka komandan upacaranya akan memberikan komando, "Kepada bendera merah garis garis putih, setrip biru pinggirnya bintang bintang putih banyak sekali.. Hormaaat graakk!!"
    
    Malah Repot kaaannn..
    `
  },
]

export default Politik