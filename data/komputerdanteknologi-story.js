const KomputerdanTeknologiStory = [
  {
    id: 1,
    title: 'Masa Lalu Sebelum ada Media Sosial',
    category: 'Humor Komputer dan Teknologi',
    content: 
    `Adakah yang ingat masa lalu yang indah sebelum ada Facebook, Instagram, dan Twitter?

    Ketika Anda harus mengambil foto makan malam Anda, lalu filmnya dicuci lalu dicetak, lalu berkeliling ke rumah teman-teman Anda untuk menunjukkan gambar makan malam Anda kepada mereka?
    
    Tidak?
    
    Saya juga tidak.
    `
  },
  {
    id: 2,
    title: 'Permintaan Terakhir Istri',
    category: 'Humor Komputer dan Teknologi',
    content: 
    `Seorang suami bertanya kepada istrinya yang sangat sakit di rumah sakit:

    "Katakan apa keinginan terakhirmu?"
    
    "Tidak ada, aku hanya ingin memeriksa statusku di Facebook."
    `
  },
  {
    id: 3,
    title: 'Terdampar Beberapa Bulan di Pulau Kecil',
    category: 'Humor Komputer dan Teknologi',
    content: 
    `Seseorang yang compang-camping, terdampar selama beberapa bulan di sebuah pulau kecil di tengah Samudra Pasifik suatu hari, melihat sebuah botol tergeletak di pasir dengan selembar kertas di dalamnya.

    "Karena tercatat sudah lebih dari 3 bulan tidak login," tulisan di kertas itu, "kami menyesal harus menghapus akun email Anda."
    `
  },
  {
    id: 4,
    title: 'Melaporkan Gangguan Internet',
    category: 'Humor Komputer dan Teknologi',
    content: 
    `Seorang klien menelepon ke hotline penyedia layanan internet:

    "Saya punya masalah, internet tidak bisa dipakai sejak 2 hari yang lalu, baik saya maupun anak saya atau orang lain tidak dapat mengaksesnya sekarang..."
    
    "Baik Pak. Apakah Anda tahu sistem operasi di komputer Anda?"
    
    "Tentu saja saya tahu! Sistem operasinya Facebook..."
    `
  },
  {
    id: 5,
    title: 'Hukuman Bagi Peretas Komputer',
    category: 'Humor Komputer dan Teknologi',
    content: 
    `"Kemarin, untuk pertama kalinya seorang peretas dihukum karena melakukan penetrasi jaringan dan diputuskan hukuman penjara selama 12 tahun.

    Menurut data dari komputer pusat Kemenkumham, peretas itu akan segera lusa karena masa tahanannya sudah habis."
    `
  },
]

export default KomputerdanTeknologiStory