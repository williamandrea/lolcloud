const HomeStory = [
  {
    id: 1,
    title: 'Mengantri di Supermarket Saat Pandemi Covid-19',
    category: 'Humor Umum',
    content: 
    `Saya sedang mengantri di supermarket ketika seorang wanita di depan saya kentut.

      Saya marah... Tetapi sebelum saya mengatakan sesuatu, dia berbalik dan berkata,
      
      "Jika Anda mendengar itu, maka Anda tidak menjaga jarak..."
      
      Kemudian dia melanjutkan,
      
      "Dan jika Anda mencium baunya, maka masker wajah Anda tidak membantu sama sekali..."
    `
  },
  {
    id: 2,
    title: 'Ketika Kambing Berdamai dengan Singa',
    category: 'Humor Umum',
    content: 
    `Alkisah, Jono dan istrinya pergi ke kebun binatang, lalu melihat sesuatu yang menarik di sebuah kandang. Di situ terlihat seekor kambing yang tinggal bersama dengan seekor singa di dalam sebuah kandang. Tampak si kambing dengan santai makan daun-daunan yang sudah disiapkan di keranjang, sementara singa dengan tenang sibuk dengan aktivitasnya sendiri.

      Melihat hal itu, istri Jono berkata, "Lihat Pa, apa Papa gak sadar, setiap kali kita ke sini, itu adalah sebuah pelajaran bagi kita, di mana makhluk hidup bisa berdampingan dan berdamai meskipun secara hukum alam seharusnya bermusuhan, karena salah satunya merupakan pemangsa yang lain."
      
      Jono juga melihat dengan takjub, "Betul, Ma. Papa juga baru menyadari sekarang, ini sebuah pelajaran bagaimana kita juga bisa berdamai dengan keadaan, sakit penyakit, ancaman lain, yang bisa membahayakan hidup kita."
      
      Tak jauh dari mereka, ada petugas kebun binatang sedang melakukan perawatan rutin di kandang itu, lalu Jono bertanya, "Mas, siapa yang melatih singa ini kok bisa seperti itu?"
      
      Petugas menjawab, "Kami tidak melatih dan tidak melakukan apapun."
      
      Jono penasaran, lalu bertanya, "Lalu bagaimana kok setiap kami ke sini, singa dan kambing ini baik-baik saja? Apa rahasianya?"
      
      Petugas menjawab, "Oh, itu ya. Jadi begini, setiap pagi sebelum membuka kebun binatang ini untuk umum, kami membersihkan kandang, dan tulang belulang kambing yang kemarin kami bersihkan, lalu kami isi ulang dengan kambing baru..."
    `
  },
  {
    id: 3,
    title: 'Doa Memindahkan Ibukota Negara',
    category: 'Humor Anak-anak',
    content: 
    `Seorang ibu memperhatikan anak perempuan kecilnya sedang berdoa.

      "Tolong, Tuhan," gadis kecil itu terus berkata. "Berkatilah ayah dan ibuku dan jadikan Melaka sebagai ibu kota Malaysia."
      
      "Kenapa kamu membuat permintaan aneh seperti itu?" tanya sang ibu.
      
      "Karena itu yang saya tulis dalam ujian Geografi saya pagi tadi!"
    `
  },
  {
    id: 4,
    title: 'Memiliki Ingatan Sangat Lemah',
    category: 'Humor Ekonomi dan Bisnis',
    content: 
    `Ingatan saya sangat buruk sehingga menyebabkan saya kehilangan pekerjaan.

      Namun, saat ini saya masih bekerja.
      
      Saya hanya tidak ingat saya bekerja di mana.
    `
  },
  {
    id: 5,
    title: 'Masa Lalu Sebelum ada Media Sosial',
    category: 'Humor Komputer dan Teknologi',
    content: 
    `Adakah yang ingat masa lalu yang indah sebelum ada Facebook, Instagram, dan Twitter?

      Ketika Anda harus mengambil foto makan malam Anda, lalu filmnya dicuci lalu dicetak, lalu berkeliling ke rumah teman-teman Anda untuk menunjukkan gambar makan malam Anda kepada mereka?
      
      Tidak?
      
      Saya juga tidak.
    `
  },
]

export default HomeStory