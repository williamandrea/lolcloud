const BahasaDaerahStory = [
  {
    id: 1,
    title: 'Carane Nolak Goda Sawektu Dinas Luar',
    category: 'Humor Bahasa Daerah',
    content: 
    `Penting diwoco karo bapak-bapak sing alim. Nek pas lagi neng hotel utowo cafe bengi-bengi ono cewek semlohay sing ngajak, kudu ngerti carane nek meh nolak secara halus nanging efektif..

    "Piyambak'an Pak..?"
    
    "Ho'o... ijen aku...!"
    
    "Ibu mboten ndherek Pak...??"
    
    "Ora.. Bojoku neng ngomah..!!"
    
    "Nopo badhe kulo kancani..?"
    
    "Kancani opo..?"
    
    "Tilem...!!"
    
    Opo kowe isoh luwih seko bojoku...?"
    
    "Njenengan ngersak'ke pripun mawon kulo saged...!"
    
    "Gur kuwi...? Ora ono istimewane meneh..?"
    
    "Lha istimewane ibu kados nopo Pak...?"
    
    "Ora mbayar...!!"
    `
  },
  {
    id: 2,
    category: 'Humor Bahasa Daerah',
    title: 'Cukur Ing Salon Modern',
    content: 
    `Mbah Kakung potong ing salon modern

    Pelayan: "Ngersakke nopo mbah?"
    
    Mbah kakung: "Ajeng cukur mas, soale ajeng reunian niki rambute pun dowo selak sumuk."
    
    Pelayan: "Nggih gampil mbah, ning sakderenge rambute dikramasi riyin nggih ben resik.."
    
    Mbah kakung: "Pun manut mawon kulo mas."
    
    Mbah kakung seneng banget lagi pisan iki dikramasi karo dipijeti sirahe. Bareng wis rampung.
    
    Pelayan: "Mbah niki rambute simbah pun putih kabeh disemir ireng riyin nggih ben ketok mundak enom melih."
    
    Mbah kakung: "Nggih mas saene mawon kulo manut."
    
    Bareng rampung di semir mbah kakung ketok gagah bali enom meneh.
    
    Pelayan: "Terus sakniki simbah ngersakke potong model pripun, ten mriki pun ahli cukur sedoyo segala model saget mbah."
    
    Mbah kakung: "Kulo pengin gundul mawon kersane ten sirah entheng."
    
    Pelayan: "!!!!!!!?!??!??!??!??!"
    `
  },
  {
    id: 3,
    category: 'Humor Bahasa Daerah',
    title: 'Gejala Tanda-tanda Tuwo',
    content: 
    `Waspada gejala T3 (tanda-tanda Tuwo)

    1. Rambut wis ora ireng
    2. Sirah gampang puyeng.
    3. Kuping ora mireng
    4. Mata rada blereng
    5. Dhahar ora nyamleng
    6. Sikil krasa kemeng
    7. Nek ngomong ora mudheng
    8. Nek cekelan ora kenceng.
    9. Nek sare gereng-gereng
    
    Tapiiii... Nek dolanan Internet isih gayeng...
    `
  },
  {
    id: 4,
    category: 'Humor Bahasa Daerah',
    title: 'Ukuran Busana Bahasa Jawa',
    content: 
    `Tahukah Anda, yang ada di toko busana ukuran pakaian, S, M, L, XL, XXL, LL, LLL, Itu berasal dari bahasa Jawa?


    S = SÃShSÃShK (sempit)
    M = MÃShTHÃShTHÃShT (ngepas)
    L = LONGGAR ( besar)
    XL = XOYO LONGGAR (makin besar)
    XXL = XAN XOYO LONGGAR
    LL = LHAKOK LONGGAR. ..?
    LLL = LHOOOO, LUWIH LONGGAR
    LLL SSM = LOOHH LUWIH LONGGAR SUWE SUWE MLOROOOT ..!!!
    `
  },
  {
    id: 5,
    category: 'Humor Bahasa Daerah',
    title: 'Nyang-nyangan Rego Becak',
    content: 
    `Ceritane mbah putri bar blonjo neng pasar Beringharjo, mulihe pengin numpak becak. Nyang-nyangan karo tukang becake.

    Simbah: "Pak, 5.000,- purun nggih ?".
    
    Tukang becak: "Oh dereng saget, 10.000 mawon mbah.."
    
    Simbah : "Emoh ah, 5.000 wae to...!!! Mengko tak duduhi dalane, ben cepet tekan omahku yoo..."
    
    Tukang becak: "Mboten saget mbah... Nek 10.000 nggih kulo purun.."
    
    Mangkel mergo tukang becake ra gelem ngedukke rego, akhire simbah putri ngalah, karo munggah lungguh becak.
    
    Simbah ngedumel: "Nggih pun, tak bayar 10.000.!!"
    
    Pas neng dalan...
    
    Tukang becak: "Mandap pundhi, mbah?"
    
    Simbah: (Meneng wae...)
    
    Tukang becak: "Lha niki mandap pundi, mbah...???"
    
    Simbah: "Kan wes tak omongi dek mau... Nek 5.000 mengko tak duduhi dalane... Ning nek 10.000 yo ora tak kandani omahku ngendi, GOLEKONO DEWE...!!!"
    `
  },
]

export default BahasaDaerahStory