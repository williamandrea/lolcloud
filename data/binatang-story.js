const Binatang = [
  {
    id: 1,
    title: 'Tupai Berebut Kacang',
    category: 'Humor Binatang',
    content: 
    `Dua tupai kecil berjalan di hutan. Tupai yang pertama melihat kacang dan berteriak, "Oh, lihat! Kacang!"

    Tupai kedua melompat di atasnya dan berkata, "Ini kacangku!"
    
    Tupai pertama berkata, "Itu tidak adil! Aku melihatnya dulu!"
    
    "Yah, kamu mungkin telah melihatnya, tapi aku memilikinya," bantah yang kedua.
    
    Pada saat itu, seekor tupai pengacara muncul dan berkata, "Kalian seharusnya tidak bertengkar. Biarkan saya menyelesaikan perselisihan ini."
    
    Kedua tupai mengangguk, dan si pengacara tupai berkata, "Sekarang, beri saya kacangnya."
    
    Dia memecahkan kacang menjadi dua, dan menyerahkan setengah untuk setiap tupai, berkata, "Lihat? Bodoh sekali kalian berkelahi. Sekarang perselisihan terselesaikan."
    
    Kemudian dia meraih dan berkata, "Dan untuk bayaran saya, saya akan mengambil kacangnya."
    `
  },
  {
    id: 2,
    title: 'Siput Memanjat Pohon',
    category: 'Humor Binatang',
    content: 
    `Seekor siput mulai perlahan memanjat batang pohon apel.

    Dia diawasi oleh seekor burung yang tidak bisa menahan tawa dan akhirnya berkata, "Tidak tahukah kamu, belum ada apel di pohon itu?"
    
    "Ya," kata siput, "tetapi akan ada pada saat aku tiba di sana."
    `
  },
  {
    id: 3,
    title: 'Penyelamatan Satwa di Toko Hewan Peliharaan',
    category: 'Humor Binatang',
    content: 
    `Seorang pria memasuki toko hewan peliharaan. Dia ingin membeli tikus hidup untuk memberi makan ular pitonnya. Lelaki itu melihat sangkar dengan seekor burung beo dan mulai memeriksanya. Tiba-tiba burung beo berkata,

    "Ooo, ada yang itunya kelihatan."
    
    Pria itu tersipu. Dia melihat sekeliling jika ada yang melihatnya dan mulai menutup ritsletingnya. Burung beo itu berkata lagi,
    
    "Celanamu sobek."
    
    Pria itu semakin memerah dan berusaha menutupi bagian belakang celananya dengan tangan.
    
    "Tali sepatumu tidak diikat", burung beo tidak berhenti.
    
    Pria itu membungkuk untuk mengikat tali sepatunya.
    
    "Kentut!... Oh baunya," teriak burung beo.
    
    Pria itu pergi dari toko karena malu. Pada titik ini tikus berbicara dari kandang mereka dan berkata,
    
    "Coco, terima kasih! Kamu menyelamatkan hidup kami lagi. Kamu harus tahu, kami akan menebusnya untukmu."
    `
  },
  {
    id: 4,
    title: 'Berdoa Karena Dikejar Harimau',
    category: 'Humor Binatang',
    content: 
    `Seorang pemburu dikejar harimau sampai di tepi jurang. Akhirnya ia pasrah kepada Tuhan, lalu memejamkan mata untuk berdoa.

    Setelah 10 menit, dia heran kok gak dimakan. Ia membuka matanya, ternyata si harimau ada di sebelahnya juga sedang berdoa.
    
    Tukang kayu itu sangat senang.. dan menyapa harimau: "Hai, rupanya kamu ikut berdoa bersama saya.."
    
    Harimau itu menjawab: "Iya dong, kan kita harus Berdoa dulu sebelum makan.."
    `
  },
  {
    id: 5,
    title: 'Sepasang Merpati yang Sedang Berkencan',
    category: 'Humor Binatang',
    content: 
    `Sepasang merpati berkencan dan janjian untuk bertemu di pinggiran lantai sepuluh sebuah gedung pencakar langit.

    Merpati betina itu ada di sana tepat waktu, tapi merpati jantan terlambat satu jam.
    
    "Di mana saja kau? Aku khawatir setengah mati."
    
    "Aku merasa hari ini sangat cerah dan menyenangkan, jadi aku memutuskan untuk ke sini berjalan kaki."
    `
  },
]

export default Binatang