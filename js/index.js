import HomeStory from '../data/home-story.js'
import UmumStory from '../data/umum-story.js'
import EkonomidanBisnisStory from '../data/ekonomidanbisnis-story.js'
import PolitikStory from '../data/politik-story.js'
import KomputerdanTeknologiStory from '../data/komputerdanteknologi-story.js'
import BinatangStory from '../data/binatang-story.js'
import BahasaDaerahStory from '../data/bahasadaerah-story.js'

document.addEventListener('DOMContentLoaded', () => {
  renderNav()
  renderPage()
})

const renderNav = () => {
  let elements = document.querySelectorAll('.sidenav')
  M.Sidenav.init(elements, {
    onOpenStart: () => {
      document.querySelector('nav').classList.remove('sticky-nav')
    },
    onCloseEnd: () => {
      document.querySelector('nav').classList.add('sticky-nav')
    }
  })

  document.querySelector('#logo-container').addEventListener('click', () => {
    renderHome()
  })

  loadNav()
}

const loadNav = () => {
  const xhttp = new XMLHttpRequest()
  xhttp.onreadystatechange = function(){
    if (this.readyState == 4){
      if (this.status != 200) return
      document.querySelectorAll('.topnav, .sidenav').forEach((element) => {
        element.innerHTML = xhttp.responseText
      })
      document.querySelectorAll('.topnav a, .sidenav a').forEach((element) => {
        element.addEventListener('click', (event) => {
          const sidenav = document.querySelector('.sidenav')
          M.Sidenav.getInstance(sidenav).close()
          const page = event.target.getAttribute('href').substr(1)
          loadPage(page)
        })
      })
    }
  }
  xhttp.open('GET', 'nav.html', true)
  xhttp.send()
}

const loadPage = (page) => {
  if (page == 'home') renderHome()
  else if (page == 'discover') renderDiscover(1)
  else if (page == 'about') renderAbout()
  else if (page == 'contact') renderContact()
}

const renderHome = () => {
  const xhttp = new XMLHttpRequest()
  xhttp.onreadystatechange = function(){
    if (this.readyState == 4){
      const bodyContent = document.querySelector('#body-content')
      if (this.status == 200){
        const content = xhttp.responseText
        const template = Handlebars.compile(content)
        const Home = template(HomeStory)
        bodyContent.innerHTML = Home
      }else if (this.status == 404){
        content.innerHTML = '<p>Halaman tidak ditemukan.</p>'
      }else {
        content.innerHTML = '<p>Ups.. Halaman tidak dapat diakses.</p>'
      }
    }
  }
  xhttp.open('GET', '../pages/home.hbs', true)
  xhttp.send()
}

const renderPage = () => {
  let page = window.location.hash.substr(1)
  if (page == '') page = 'home'
  loadPage(page)
}

const renderSelect = () => {
  const elements = document.querySelectorAll('select')
  const instance = M.FormSelect.init(elements)

  const selectCategory = document.querySelector('#select-category')
  selectCategory.addEventListener('change', function() {
    renderDiscover(this.value)
  })
}

const renderDiscover = (selectedValue) => {
  const xhttp = new XMLHttpRequest()
  xhttp.onreadystatechange = function(){
    if (this.readyState == 4){
      const bodyContent = document.querySelector('#body-content')
      if (this.status == 200){
        const content = xhttp.responseText
        const template = Handlebars.compile(content)
        if (selectedValue == 1){
          const temp = template(UmumStory)
          bodyContent.innerHTML = temp
        }else if (selectedValue == 2){
          const temp = template(EkonomidanBisnisStory)
          bodyContent.innerHTML = temp
        }else if (selectedValue == 3){
          const temp = template(PolitikStory)
          bodyContent.innerHTML = temp
        }else if (selectedValue == 4){
          const temp = template(KomputerdanTeknologiStory)
          bodyContent.innerHTML = temp
        }else if (selectedValue == 5){
          const temp = template(BinatangStory)
          bodyContent.innerHTML = temp
        }else if (selectedValue == 6){
          const temp = template(BahasaDaerahStory)
          bodyContent.innerHTML = temp
        }
      }else if (this.status == 404){
        content.innerHTML = '<p>Halaman tidak ditemukan.</p>'
      }else {
        content.innerHTML = '<p>Ups.. Halaman tidak dapat diakses.</p>'
      }
      renderSelect()
    }
  }
  xhttp.open('GET', '../pages/discover.hbs', true)
  xhttp.send()
}

const renderAbout = () => {
  const xhttp = new XMLHttpRequest()
  xhttp.onreadystatechange = function(){
    if (this.readyState == 4){
      const bodyContent = document.querySelector('#body-content')
      if (this.status == 200){
        const content = xhttp.responseText
        bodyContent.innerHTML = content
      }else if (this.status == 404){
        content.innerHTML = '<p>Halaman tidak ditemukan.</p>'
      }else {
        content.innerHTML = '<p>Ups.. Halaman tidak dapat diakses.</p>'
      }
    }
  }
  xhttp.open('GET', '../pages/about.hbs', true)
  xhttp.send()
}

const renderContact = () => {
  const xhttp = new XMLHttpRequest()
  xhttp.onreadystatechange = function(){
    if (this.readyState == 4){
      const bodyContent = document.querySelector('#body-content')
      if (this.status == 200){
        const content = xhttp.responseText
        bodyContent.innerHTML = content
      }else if (this.status == 404){
        content.innerHTML = '<p>Halaman tidak ditemukan.</p>'
      }else {
        content.innerHTML = '<p>Ups.. Halaman tidak dapat diakses.</p>'
      }
    }
  }
  xhttp.open('GET', '../pages/contact.hbs', true)
  xhttp.send()
}