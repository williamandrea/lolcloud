const CACHE_NAME = 'lolcloud-v4'
const urlToCache = [
  '/css/materialize.min.css',
  '/css/style.css',

  '/data/bahasadaerah-story.js',
  '/data/binatang-story.js',
  '/data/ekonomidanbisnis-story.js',
  '/data/home-story.js',
  '/data/komputerdanteknologi-story.js',
  '/data/politik-story.js',
  '/data/umum-story.js',

  '/fonts/NotoSans.ttf',

  '/icons/MaterialIcons.ttf',
  '/icons/icon-72.png',
  '/icons/icon-96.png',
  '/icons/icon-128.png',
  '/icons/icon-144.png',
  '/icons/icon-192.png',
  '/icons/icon-256.png',
  '/icons/icon-384.png',
  '/icons/icon-512.png',


  '/images/fb.png',
  '/images/ig.png',
  '/images/tb.png',
  '/images/tw.png',
  '/images/yt.png',

  '/js/handlebars.min.js',
  '/js/index.js',
  '/js/materialize.min.js',

  '/pages/about.hbs',
  '/pages/contact.hbs',
  '/pages/discover.hbs',
  '/pages/home.hbs',

  '/',
  '/index.html',
  '/manifest.json',
  '/nav.html',
  '/service-worker.js'
]

self.addEventListener('install', (event) => {
  event.waitUntil(
    caches.open(CACHE_NAME).then((cache) => {
      return cache.addAll(urlToCache)
    })
  )
})

self.addEventListener('fetch', (event) => {
  event.respondWith(
    caches
      .match(event.request, {cacheName: CACHE_NAME})
      .then((response) => {
        if (response){
          console.log('ServiceWorker: Gunakan aset dari cache: ', response.url)
          return response
        }
        console.log('ServiceWorker: Memuat aset dari server: ', event.request.url)
        return fetch(event.request)
      })
  )
})

self.addEventListener('activate', (event) => {
  event.waitUntil(
    caches.keys().then((cacheNames) => {
      return Promise.all(
        cacheNames.map((cacheName) => {
          if (cacheName != CACHE_NAME){
            console.log('ServiceWorker: cache', cacheName, 'dihapus.')
            return caches.delete(cacheName)
          }
        })
      )
    })
  )
})